<div>
    <div class="flex flex-row items-center justify-end space-x-2">
        <button type="button" class="inline-flex items-center px-4 py-2 space-x-2 text-xs font-semibold tracking-widest text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25" onclick="toggleElement('create')">
            <i class="fas fa-plus"></i> <p>CREATE</p>
        </button>
    </div>
    
    <div class="relative py-2">
        <svg width="20" height="20" fill="currentColor" class="absolute text-gray-400 transform -translate-y-1/2 left-3 top-1/2">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
        </svg>
        <input wire:model="search" class="w-full py-2 pl-10 text-sm text-black placeholder-gray-500 border border-gray-200 rounded-md focus:border-light-blue-500 focus:ring-1 focus:ring-light-blue-500 focus:outline-none" type="text" placeholder="Search Products" />
    </div>

    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <div class="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
            <table class="min-w-full divide-y divide-gray-200">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                    Product
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                    CATEGORY
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                    Links
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                    Options
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach ($products as $product)
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                    <div class="flex items-center">
                        <div class="flex-shrink-0 w-10 h-10">
                        <img class="w-auto h-10" src="{{is_null($product->image) ? asset('product.png') : asset('/products/'.$product->image)}}" alt="">
                        </div>
                        <div class="ml-4">
                            <div class="text-xs font-medium text-gray-900 uppercase">
                                {{$product->name}}
                            </div>
                        </div>
                    </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="text-xs text-gray-900 uppercase">{{$product->category == 1 ? 'FEEDS' : ($product->category == 2 ? 'POINTING SUPPLEMENTS' : ($product->category == 3 ? 'SUPPLEMENTS' : ($product->category == 4 ? 'SHAMPOO' : ($product->category == 5 ? 'DISINFECTANTS' : ($product->category == 6 ? 'ANTIBACTERIALS' : 'BUNDLES')))))}}</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="flex flex-row items-center justify-center space-x-2">
                            <div class="flex flex-col text-center focus:outline-none">
                                <button class="text-gray-700"  onclick="toggleElement('videos{{$product->id}}')"><i class="fas fa-video"></i></button>
                                <p class="text-xs text-gray-500">Videos</p>
                            </div>
                            <div class="flex flex-col text-center focus:outline-none">
                                <button class="text-gray-700"  onclick="toggleElement('comments{{$product->id}}')"><i class="fas fa-comments"></i></button>
                                <p class="text-xs text-gray-500">Comments</p>
                            </div>
                            <div class="flex flex-col text-center focus:outline-none {{is_null($product->lazada) ? 'hidden' : ''}}">
                                <a href="{{$product->lazada}}" class="text-blue-700" target="_blank"><i class="fas fa-shopping-bag"></i></a>
                                <p class="text-xs text-gray-500">Lazada</p>
                            </div>
                            <div class="flex flex-col text-center focus:outline-none {{is_null($product->shopee) ? 'hidden' : ''}}">
                                <a href="{{$product->shopee}}" class="text-yellow-600" target="_blank"><i class="fas fa-shopping-bag"></i></a>
                                <p class="text-xs text-gray-500">Shopee</p>
                            </div>
                            <div class="flex flex-col text-center focus:outline-none {{is_null($product->website) ? 'hidden' : ''}}">
                                <a href="{{$product->website}}" class="text-indigo-900" target="_blank"><i class="fas fa-globe"></i></a>
                                <p class="text-xs text-gray-500">Website</p>
                            </div>
                        </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="flex flex-row items-center justify-center space-x-2">
                            <div class="flex flex-col text-center">
                                <button type="button" class="text-gray-600 focus:outline-none" onclick="toggleElement('update{{$product->id}}')"><i class="fas fa-edit"></i></button>
                                <p class="text-xs text-gray-500">Edit</p>
                            </div>
                            <div class="flex flex-col text-center">
                                <form method="POST" action="{{route('products.destroy',$product->id)}}">
                                @csrf
                                @method('delete')
                                <button type="submit" class="text-red-900 focus:outline-none"><i class="fas fa-trash"></i></button>
                                <p class="text-xs text-gray-500">Delete</p>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>

                <div id="update{{$product->id}}" class="fixed inset-0 z-50 hidden overflow-hidden" aria-labelledby="slide-over-title" role="dialog" aria-modal="true">
                    <div class="absolute inset-0 overflow-hidden">
                    <div class="absolute inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
                
                    <div class="fixed inset-y-0 right-0 flex max-w-full pl-10">
            
                        <div class="relative w-screen max-w-md">
                
                        <div class="flex flex-col h-full py-6 overflow-y-scroll bg-white shadow-xl">
                            <div class="inline-flex items-center justify-between px-4 sm:px-6">
                            <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">
                                UPDATE PRODUCT
                            </h2>
                            <button type="button" class="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white" onclick="toggleElement('update{{$product->id}}')">
                                <span class="sr-only">Close panel</span>
                                <!-- Heroicon name: outline/x -->
                                <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                            </div>
                            <div class="relative flex-1 px-4 mt-6 sm:px-6">
                            <div class="absolute inset-0 px-4 sm:px-6">
                                <div class="h-full border-2 border-gray-200 border-dashed" aria-hidden="true">
                                    <form method="POST" action="{{route('products.update',$product->id)}}" enctype="multipart/form-data">
                                        @csrf
                                        @method('patch')
                                        <dl>
                                            <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Name
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <x-jet-input id="name" class="block w-full" type="text" name="name" value="{{$product->name}}" required autofocus />
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Lazada
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <x-jet-input id="lazada" class="block w-full" type="text" name="lazada" value="{{$product->lazada}}" autofocus />
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Shopee
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <x-jet-input id="shopee" class="block w-full" type="text" name="shopee" value="{{$product->shopee}}" autofocus />
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Website
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <x-jet-input id="website" class="block w-full" type="text" name="website" value="{{$product->website}}" autofocus />
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Description
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <textarea id="description" name="description" class="w-full text-sm border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">{{$product->description}}</textarea>
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Use
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <textarea id="usage" name="usage" class="w-full text-sm border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">{{$product->usage}}</textarea>
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Product Image
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                <input id="image" name="image" type="file" class="items-center block w-full px-4 py-2 mt-2 mr-2 text-xs font-semibold tracking-widest text-center text-gray-700 uppercase transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50" accept="image/*">
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Category
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <select id="category" name="category" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-500 focus:ring-opacity-50">
                                                        <option id="category" name="category" value="1" {{ $product->category == 1 ? 'selected' : '' }}>Feeds</option>
                                                        <option id="category" name="category" value="2" {{ $product->category == 2 ? 'selected' : '' }}>Pointing Supplements</option>
                                                        <option id="category" name="category" value="3" {{ $product->category == 3 ? 'selected' : '' }}>Supplements</option>
                                                        <option id="category" name="category" value="4" {{ $product->category == 4 ? 'selected' : '' }}>Shampoo</option>
                                                        <option id="category" name="category" value="5" {{ $product->category == 5 ? 'selected' : '' }}>Disinfectants</option>
                                                        <option id="category" name="category" value="6" {{ $product->category == 6 ? 'selected' : '' }}>Antibacterials</option>
                                                        <option id="category" name="category" value="7" {{ $product->category == 7 ? 'selected' : '' }}>Bundles</option>
                                                    </select>
                                                </dd>
                                            </div>
                                            <div class="py-2 m-2 border-t-2 border-gray-200 border-dashed">
                                                <button type="submit" class="items-center w-full px-4 py-2 text-xs font-semibold tracking-widest text-center text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25">
                                                    UPDATE
                                                </button>
                                            </div>
                                        </dl>
                                    </form>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div id="videos{{$product->id}}" class="fixed inset-0 z-50 hidden overflow-hidden" aria-labelledby="slide-over-title" role="dialog" aria-modal="true">
                    <div class="absolute inset-0 overflow-hidden">
                    <div class="absolute inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
                
                    <div class="fixed inset-y-0 right-0 flex max-w-full pl-10">
            
                        <div class="relative w-screen max-w-md">
                
                        <div class="flex flex-col h-full py-6 overflow-y-scroll bg-white shadow-xl">
                            <div class="inline-flex items-center justify-between px-4 sm:px-6">
                            <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">
                                PRODUCT VIDEOS
                            </h2>
                            <button type="button" class="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white" onclick="toggleElement('videos{{$product->id}}')">
                                <span class="sr-only">Close panel</span>
                                <!-- Heroicon name: outline/x -->
                                <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                            </div>
                            <div class="relative flex-1 px-4 mt-6 sm:px-6">
                            <div class="absolute inset-0 px-4 sm:px-6">
                                <div class="h-full overflow-y-auto border-2 border-gray-200 border-dashed" aria-hidden="true">
                                    <form method="POST" action="{{route('products_videos.store')}}" enctype="multipart/form-data">
                                        @csrf
                                        <dl>
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Title
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <x-jet-input id="title" class="block w-full" type="text" name="title" required autofocus />
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Video
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <input id="video" name="video" type="file" class="items-center block w-full px-4 py-2 mt-2 mr-2 text-xs font-semibold tracking-widest text-center text-gray-700 uppercase transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50" accept="video/*" required>
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-white border-b-2 border-gray-200 border-dashed sm:px-6">
                                                <div class="flex items-center justify-end">
                                                    <button type="submit" class="items-center w-full px-4 py-2 text-xs font-semibold tracking-widest text-center text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25">
                                                        CREATE
                                                    </button>
                                                </div>
                                            </div>
                                        </dl>
                                    </form>
                                    <dl class="flex flex-col overflow-y-auto border-gray-200 border-dashed">
                                        <div class="text-center border-b-2 border-gray-200 border-dashed bg-gray-50 focus:outline-none">{{$product->name}}</div>
                                        <div class="w-full h-full overflow-y-auto">
                                            <div class="grid items-center justify-center w-full h-full grid-cols-1 border-b-2 border-gray-200 border-dashed">
                                                @foreach($product->videos as $vid)
                                                    <div class="grid items-center justify-center w-full h-full grid-cols-3 border-b-2 border-gray-200 border-dashed">
                                                        <div class="flex items-center justify-between col-span-2 p-2 uppercase">
                                                            <h1 class="text-xs">{{$vid->title}}</h1>
                                                        </div>
                                                        <div class="flex flex-row flex-wrap items-center justify-center space-x-2 text-xs">
                                                            <button type="button" class="text-gray-600 focus:outline-none" onclick="toggleElement('view_video{{$vid->id}}')"><i class="fas fa-play"></i></button>
                                                            <button type="button" class="text-gray-600 focus:outline-none" onclick="toggleElement('edit_video{{$vid->id}}')"><i class="fas fa-edit"></i></button>
                                                            <form method="POST" action="{{route('products_videos.destroy',$vid->id)}}">
                                                                @csrf
                                                                @method('delete')
                                                                    <button type="submit" class="text-red-600 hover:text-red-900 focus:outline-none"><i class="fas fa-trash"></i></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div id="view_video{{$vid->id}}" class="hidden w-full h-full border-t-2 border-gray-200 border-dashed bg-gray-50">
                                                        <video class="w-full h-full p-6 border-gray-900 border-1" controls>
                                                            <source src="{{asset('product_vids/'.$vid->video)}}" type="video/mp4">
                                                        </video>
                                                    </div>
                                                    <div id="edit_video{{$vid->id}}" class="hidden w-full h-full bg-white border-t-2 border-gray-200 border-dashed">
                                                        <form method="POST" action="{{route('products_videos.update',$vid->id)}}" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('patch')
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        <div class="flex flex-col items-center justify-between p-2 space-y-2 uppercase">
                                                            <input type="text" name="title" class="w-full text-xs border border-gray-900 rounded" value="{{$vid->title}}" placeholder="Title" required>
                                                            <input id="video" name="video" type="file" class="items-center block w-full px-4 py-2 text-xs font-semibold tracking-widest text-center text-gray-700 uppercase transition duration-150 ease-in-out bg-white border border-gray-900 rounded hover:text-gray-500 focus:outline-none active:text-gray-800 active:bg-gray-50" accept="video/*">
                                                            <button type="submit" class="items-center w-full p-2 text-xs font-semibold tracking-widest text-center text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25">
                                                                UPDATE
                                                            </button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div id="comments{{$product->id}}" class="fixed inset-0 z-50 hidden overflow-hidden" aria-labelledby="slide-over-title" role="dialog" aria-modal="true">
                    <div class="absolute inset-0 overflow-hidden">
                    <div class="absolute inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
                
                    <div class="fixed inset-y-0 right-0 flex max-w-full pl-10">
            
                        <div class="relative w-screen max-w-md">
                
                        <div class="flex flex-col h-full py-6 overflow-y-scroll bg-white shadow-xl">
                            <div class="inline-flex items-center justify-between px-4 sm:px-6">
                            <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">
                                PRODUCT REVIEWS
                            </h2>
                            <button type="button" class="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white" onclick="toggleElement('comments{{$product->id}}')">
                                <span class="sr-only">Close panel</span>
                                <!-- Heroicon name: outline/x -->
                                <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                            </div>
                            <div class="relative flex-1 px-4 mt-6 sm:px-6">
                            <div class="absolute inset-0 px-4 sm:px-6">
                                <div class="h-full overflow-y-auto border-2 border-gray-200 border-dashed" aria-hidden="true">
                                    <form method="POST" action="{{route('products_comments.store')}}">
                                        @csrf
                                        <dl>
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Reviewer
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <x-jet-input id="name" class="block w-full" type="text" name="name" required autofocus />
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                <dt class="mt-3 text-sm font-medium text-gray-500">
                                                Comment
                                                </dt>
                                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                    <textarea id="comment" name="comment" class="w-full text-sm border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"></textarea>
                                                </dd>
                                            </div>
                                            <div class="px-4 py-2 bg-white border-b-2 border-gray-200 border-dashed sm:px-6">
                                                <div class="flex items-center justify-end">
                                                    <button type="submit" class="items-center w-full px-4 py-2 text-xs font-semibold tracking-widest text-center text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25">
                                                        CREATE
                                                    </button>
                                                </div>
                                            </div>
                                        </dl>
                                    </form>
                                    <dl class="flex flex-col overflow-y-auto border-gray-200 border-dashed">
                                        <div class="text-center border-b-2 border-gray-200 border-dashed bg-gray-50 focus:outline-none">{{$product->name}}</div>
                                        <div class="w-full h-full overflow-y-auto">
                                            <div class="grid items-center justify-center w-full h-full grid-cols-1 border-b-2 border-gray-200 border-dashed">
                                                @foreach($product->reviews as $review)
                                                    <div class="grid items-center justify-center w-full h-full grid-cols-3 border-b-2 border-gray-200 border-dashed">
                                                        <div class="flex flex-col items-start justify-between col-span-2 p-2 uppercase">
                                                            <h1 class="text-xs font-bold">{{$review->name}}</h1>
                                                            <small class="text-xs">
                                                                {{$review->comment}}
                                                            </small>
                                                        </div>
                                                        <div class="flex flex-row flex-wrap items-center justify-center space-x-2 text-xs">
                                                            <button type="button" class="text-gray-600 focus:outline-none" onclick="toggleElement('edit_comment{{$review->id}}')"><i class="fas fa-edit"></i></button>
                                                            <form method="POST" action="{{route('products_comments.destroy',$review->id)}}">
                                                                @csrf
                                                                @method('delete')
                                                                    <button type="submit" class="text-red-600 hover:text-red-900 focus:outline-none"><i class="fas fa-trash"></i></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div id="edit_comment{{$review->id}}" class="hidden w-full h-full bg-white border-t-2 border-gray-200 border-dashed">
                                                        <form method="POST" action="{{route('products_comments.update',$review->id)}}">
                                                        @csrf
                                                        @method('patch')
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        <div class="flex flex-col items-center justify-between p-2 space-y-2 uppercase">
                                                            <input type="text" id="name" name="name" class="w-full text-xs border border-gray-900 rounded" value="{{$review->name}}" placeholder="Reviewer's Name" required>
                                                            <textarea id="comment" name="comment" class="items-center block w-full px-4 py-2 text-xs font-semibold tracking-widest text-gray-700 uppercase transition duration-150 ease-in-out bg-white border border-gray-900 rounded hover:text-gray-500 focus:outline-none active:text-gray-800 active:bg-gray-50" placeholder="Reviewer's Comment">{{$review->comment}}</textarea>
                                                            <button type="submit" class="items-center w-full p-2 text-xs font-semibold tracking-widest text-center text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25">
                                                                UPDATE
                                                            </button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>

    <div id="create" class="fixed inset-0 z-50 hidden overflow-hidden" aria-labelledby="slide-over-title" role="dialog" aria-modal="true">
        <div class="absolute inset-0 overflow-hidden">
        <div class="absolute inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
    
        <div class="fixed inset-y-0 right-0 flex max-w-full pl-10">

            <div class="relative w-screen max-w-md">
    
            <div class="flex flex-col h-full py-6 overflow-y-scroll bg-white shadow-xl">
                <div class="inline-flex items-center justify-between px-4 sm:px-6">
                <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">
                    CREATE PRODUCT
                </h2>
                <button type="button" class="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white" onclick="toggleElement('create')">
                    <span class="sr-only">Close panel</span>
                    <!-- Heroicon name: outline/x -->
                    <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
                </div>
                <div class="relative flex-1 px-4 mt-6 sm:px-6">
                <div class="absolute inset-0 px-4 sm:px-6">
                    <div class="h-full border-2 border-gray-200 border-dashed" aria-hidden="true">
                        <form method="POST" action="{{route('products.store')}}" enctype="multipart/form-data">
                            @csrf
                            <dl>
                                <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Name
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <x-jet-input id="name" class="block w-full" type="text" name="name" required autofocus />
                                    </dd>
                                </div>
                                <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Lazada
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <x-jet-input id="lazada" class="block w-full" type="text" name="lazada" autofocus />
                                    </dd>
                                </div>
                                <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Shopee
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <x-jet-input id="shopee" class="block w-full" type="text" name="shopee" autofocus />
                                    </dd>
                                </div>
                                <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Website
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <x-jet-input id="website" class="block w-full" type="text" name="website" autofocus />
                                    </dd>
                                </div>
                                <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Description
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <textarea id="description" name="description" class="w-full text-sm border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"></textarea>
                                    </dd>
                                </div>
                                <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Use
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <textarea id="usage" name="usage" class="w-full text-sm border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"></textarea>
                                    </dd>
                                </div>
                                <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Product Image
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    <input id="image" name="image" type="file" class="items-center block w-full px-4 py-2 mt-2 mr-2 text-xs font-semibold tracking-widest text-center text-gray-700 uppercase transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50" accept="image/*">
                                    </dd>
                                </div>
                                <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt class="mt-3 text-sm font-medium text-gray-500">
                                    Category
                                    </dt>
                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <select id="category" name="category" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-500 focus:ring-opacity-50">
                                            <option id="category" name="category" value="1" >Feeds</option>
                                            <option id="category" name="category" value="2" >Pointing Supplements</option>
                                            <option id="category" name="category" value="3" >Supplements</option>
                                            <option id="category" name="category" value="4" >Shampoo</option>
                                            <option id="category" name="category" value="5" >Disinfectants</option>
                                            <option id="category" name="category" value="6" >Antibacterials</option>
                                            <option id="category" name="category" value="7" >Bundles</option>
                                        </select>
                                    </dd>
                                </div>
                                <div class="py-2 m-2 border-t-2 border-gray-200 border-dashed">
                                    <button type="submit" class="items-center w-full px-4 py-2 text-xs font-semibold tracking-widest text-center text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25">
                                        CREATE
                                    </button>
                                </div>
                            </dl>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>