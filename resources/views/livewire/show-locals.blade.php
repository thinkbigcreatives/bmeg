<div>
    <div class="relative px-6 py-8 lg:px-32 pro:px-6">
        <div class="flex flex-col items-center justify-center w-full h-full lg:space-x-6 lg:items-start lg:justify-start pro:items-center pro:justify-center lg:flex-row">
            <div class="flex flex-col items-center justify-center w-full h-full space-y-2 lg:w-1/4">
                <div class="flex flex-col items-center justify-center w-auto h-auto">
                    <a href="{{ auth()->check() ? route('dashboard') : url('/') }}">
                        <img src="{{asset('logo.png')}}" class="w-auto h-20 transform hover:scale-105">
                    </a>
                    <h1 class="py-2 text-3xl text-center text-red-900 font-futura stroke-white">REGIONAL ENDORSERS</h1>
                </div>
                <div class="hidden w-full my-10 overflow-y-auto lg:block xl:block no-scrollbar h-96">
                    <div class="grid grid-cols-1 gap-6 pt-2 text-center">
                        <div class="p-2 text-xs bg-red-900 focus:outline-none hover:text-yellow-300 font-futura {{is_null($locID) ? 'border-b-2 border-yellow-300 text-yellow-300' : 'text-white'}}">
                            <a href="{{route('regionals.index')}}">ALL</a>
                        </div>
                        @foreach($rlocations as $rlocation)
                        <div class="p-2 text-xs text-white bg-red-900 focus:outline-none hover:text-yellow-300 font-futura {{$locID == $rlocation->id ? 'border-b-2 border-yellow-300 text-yellow-300' : 'text-white'}}">
                            <button wire:click.prevent="updateLocation({{$rlocation->id}})" class="uppercase">{{$rlocation->name}}</button>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="hidden text-xs text-white lg:block xl:block">Note: Scroll the Regional Locations to view more</div>
            </div>
            <div class="flex flex-col items-center justify-center w-full h-full space-y-2 overflow-y-auto lg:w-3/4">
                <div class="grid items-center justify-center w-full h-full grid-cols-1 gap-2 md:grid-cols-2">
                    @foreach($regionals as $regional)
                    <div class="flex flex-row items-start justify-start w-auto h-auto p-6 bg-gray-100 shadow lg:items-center rounded-xl">
                        <div class="inline-flex w-40 h-40 overflow-hidden">
                            <img src="{{is_null($regional->image) ? asset('endorser.png') : asset('/regional/'.$regional->image)}}" class="z-20 w-full h-full shadow-xl">
                        </div>
                        <div class="flex flex-col text-left">
                            <h2 class="text-lg font-extrabold text-blue-900 uppercase font-futura">{{$regional->name}}</h2>
                            <h6 class="text-sm font-bold uppercase">{{$regional->farm}}</h6>
                            <ul class="pt-2 text-xs text-left">
                                <li><i class="fas fa-map-marker-alt"></i> {{$regional->location}}</li>
                                <li class="{{is_null($regional->email) ? 'hidden' : ''}}"><i class="fas fa-at"></i> {{$regional->email}}</li>
                                <li class="{{is_null($regional->contact) ? 'hidden' : ''}}"><i class="fas fa-phone"></i> {{$regional->contact}}</li>
                                <li class="{{is_null($regional->fb) ? 'hidden' : ''}}"><a href="{{$regional->fb}}" target="_blank"><i class="fab fa-facebook-square"></i> Facebook Profile</a></li>
                                <li class="{{is_null($regional->ig) ? 'hidden' : ''}}"><a href="{{$regional->ig}}" target="_blank"><i class="fab fa-instagram-square"></i> Instagram Profile</a></li>
                                <li class="{{is_null($regional->website) ? 'hidden' : ''}}"><a href="{{$regional->website}}" target="_blank"><i class="fas fa-globe"></i> Personal Website</a></li>
                            </ul>  
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="flex items-center justify-end w-full h-full p-2 m-2">
            {{$regionals->links()}}
        </div>
    </div>
</div>
