<div>
    <div class="flex flex-row items-center justify-end">
        <button type="button" class="inline-flex items-center px-4 py-2 space-x-2 text-xs font-semibold tracking-widest text-white uppercase transition bg-gray-900 border border-transparent rounded-md hover:bg-gray-300 hover:text-gray-900 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25" onclick="toggleElement('createKMP')">
            <i class="fas fa-user-plus"></i> <p>CREATE</p>
        </button>
    </div>
    
    <div class="relative py-2">
        <svg width="20" height="20" fill="currentColor" class="absolute text-gray-400 transform -translate-y-1/2 left-3 top-1/2">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
        </svg>
        <input wire:model="search" class="w-full py-2 pl-10 text-sm text-black placeholder-gray-500 border border-gray-200 rounded-md focus:border-light-blue-500 focus:ring-1 focus:ring-light-blue-500 focus:outline-none" type="text" placeholder="Search KMP Showcases" />
    </div>
    
    <div class="flex flex-col py-4">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                <div class="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                    <tr>
                        <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                        Title
                        </th>
                        <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                        Options
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($showcases as $showcase)
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="flex-shrink-0 w-auto h-10">
                                <img class="w-auto h-10" src="{{is_null($showcase->image) ? asset('logo.png') : asset('/showcases/'.$showcase->image)}}" alt="">
                            </div>
                            <div class="ml-4">
                            <div class="text-sm font-medium text-gray-900">
                                {{$showcase->title}}
                            </div>
                            </div>
                        </div>
                        </td>
                        <td class="inline-flex px-6 py-4 text-sm font-medium text-center whitespace-nowrap">
                            <div class="flex flex-col items-center justify-center mx-2">
                                <div class="text-sm font-medium text-gray-900">
                                <form method="POST" action="{{route('showcases.update',$showcase->id)}}">
                                    @csrf
                                    @method('patch')
                                    <button type="submit"class="pr-2 text-gray-600 hover:text-gray-900 focus:outline-none"><i class="fas {{$showcase->archive == false ? 'fa-file-archive' : 'fa-archive'}}"></i></button>
                                </form>
                                </div>
                                <div class="text-xs text-gray-500">
                                    {{$showcase->archive == false ? 'Archive' : 'Activate'}}
                                </div>
                            </div>
                            <div class="flex flex-col items-center justify-center mx-2">
                                <div class="text-sm font-medium text-gray-900">
                                    <form method="POST" action="{{route('showcases.destroy',$showcase->id)}}">
                                    @csrf
                                    @method('delete')
                                        <button type="submit" class="text-red-600 hover:text-red-900 focus:outline-none"><i class="fas fa-trash"></i></button>
                                    </form>
                                </div>
                                <div class="text-xs text-gray-500">
                                Delete
                                </div>
                            </div>
                        </td>
                    </tr>   
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

    <div id="createKMP" class="fixed inset-0 z-30 hidden overflow-y-auto">
        <form method="POST" action="{{route('showcases.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition-opacity" aria-hidden="true">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
            <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            
            <div class="overflow-hidden bg-white shadow sm:rounded-lg">
                <div class="px-4 py-2 sm:px-6">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">
                    KMP of The Month
                    </h3>
                    <p class="max-w-2xl mt-1 text-sm text-gray-500">
                    Create a featured KMP of The Month Endorser.
                    </p>
                </div>
                <div class="border-t border-gray-200">
                    <dl>
                    <div class="px-4 py-2 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="mt-3 text-sm font-medium text-gray-500">
                        Title
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            <x-jet-input id="title" class="block w-full" type="text" name="title" required autofocus/>
                        </dd>
                    </div>
                    <div class="px-4 py-2 bg-white sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="mt-3 text-sm font-medium text-gray-500">
                        Image
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input id="image" name="image" type="file" class="items-center block w-full px-4 py-2 mt-2 mr-2 text-xs font-semibold tracking-widest text-center text-gray-700 uppercase transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50" accept="image/*" required>
                        </dd>
                    </div>
                    </dl>
                </div>
            </div>

            <div class="px-4 py-2 bg-gray-50 sm:px-6 sm:flex sm:flex-row-reverse">
                <button type="submit" class="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-gray-600 border border-transparent rounded-md shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 sm:ml-3 sm:w-auto sm:text-sm">
                Create
                </button>
                <button type="button" class="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-300 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm" onclick="toggleElement('createKMP')">
                Close
                </button>
            </div>
            </div>
        </div>
        </form>
    </div>
</div>
