<x-app-layout>
    <div class="py-10">
        <div class="w-screen max-w-6xl mx-auto">
            <div class="flex items-center justify-center w-full h-full">
                <div class="flex flex-col w-full lg:flex-row">
                    <div class="w-full px-4 pt-0 lg:pt-10 md:pt-0 lg:w-1/4">
                        <div class="flex flex-col items-center justify-center text-center">
                            <a href="{{ auth()->check() ? route('dashboard') : url('/') }}">
                                <img src="{{asset('logo.png')}}" class="w-auto h-20 transform hover:scale-105">
                            </a>
                            <h1 class="text-2xl text-red-900 font-futura stroke-white">LOCAL DISTRIBUTORS</h1>
                        </div>
                        <div class="hidden my-10 overflow-y-auto lg::block xl:block no-scrollbar h-80">
                            <div class="grid grid-cols-1 gap-2 pt-2 text-center">
                                <div class="p-2 text-xs text-white bg-red-900 focus:outline-none hover:text-yellow-300 font-futura">
                                    <a href="{{route('directories.index')}}">ALL</a>
                                </div>
                                @foreach($locations as $loc)
                                <div class="p-2 text-xs text-white bg-red-900 focus:outline-none hover:text-yellow-300 font-futura">
                                    <a class="uppercase" href="{{route('directories.show',$loc->id)}}">{{$loc->name}}</a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="block lg:hidden xl:hidden">
                            <div class="grid grid-cols-1 gap-2 pt-2 text-center">
                                <select id="rl_id" name="rl_id" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-500 focus:ring-opacity-50" onchange="location.replace('{{url('directories').'/'}}'+this.value)" >
                                    @foreach($locations as $loc)
                                    <option class="uppercase" id="loc_id" name="loc_id" value="{{$loc->id}}" {{ $loc->id == $id ? 'selected' : '' }}>{{$loc->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="w-full px-10 pt-10 lg:w-3/4 lg:pt-0">
                        <div class="flex items-center justify-center">
                            <div class="flex flex-col">
                                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                                    <div class="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
                                    <table class="min-w-full divide-y divide-gray-200">
                                        <thead class="bg-gray-50">
                                        <tr>
                                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                                            Operations Manager Details
                                            </th>
                                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                                            Contact Details
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-200">
                                        @foreach($directories as $directory)
                                        <tr>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="flex items-center">
                                                <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900 uppercase">
                                                    {{$directory->manager}}
                                                </div>
                                                <div class="text-xs text-gray-500 uppercase">
                                                    {{$directory->area}}
                                                </div>
                                                </div>
                                            </div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{$directory->contact}}</div>
                                                <div class="text-sm text-gray-500">{{$directory->email}}</div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </x-app-layout>