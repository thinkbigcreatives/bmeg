<?php

namespace App\Http\Controllers;

use App\Models\DirectoryLocation;
use Illuminate\Http\Request;

class DirectoryLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DirectoryLocation  $directoryLocation
     * @return \Illuminate\Http\Response
     */
    public function show(DirectoryLocation $directoryLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DirectoryLocation  $directoryLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(DirectoryLocation $directoryLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DirectoryLocation  $directoryLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DirectoryLocation $directoryLocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DirectoryLocation  $directoryLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(DirectoryLocation $directoryLocation)
    {
        //
    }
}
