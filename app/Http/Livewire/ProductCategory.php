<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductCategory extends Component
{
    public function render()
    {
        return view('livewire.product-category');
    }
}
