<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\WithPagination;
use Livewire\Component;

class ProductTable extends Component
{
    use WithPagination;

    protected $products;
    public $search = null;

    public function updating()
    {
        $this->resetPage();
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function render()
    {
        // $this->products = !is_null($this->search) ? Product::search($this->search)->orderBy('name')->paginate(5) : Product::orderBy('name')->paginate(5);
        $this->products = !is_null($this->search) ? Product::search($this->search)->orderBy('name')->get() : Product::orderBy('name')->get();

        return view('livewire.product-table', [
            'products' => $this->products,
        ]);

    }
}
