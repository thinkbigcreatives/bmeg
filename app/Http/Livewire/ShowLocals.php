<?php

namespace App\Http\Livewire;

use App\Models\Regional;
use App\Models\RegionalLocation;
use Livewire\Component;
use Livewire\WithPagination;

class ShowLocals extends Component
{
    use WithPagination;

    protected $regionals;
    
    public $locID = null;
    public $rlocations;

    public function mount() {
        $this->rlocations = RegionalLocation::orderBy('name')->get();
    }

    public function updateLocation($locID)
    {
        $this->resetPage();
        $this->locID = $locID;
    }

    public function render()
    {
        $this->regionals = is_null($this->locID) ? Regional::orderBy('name')->paginate(6) : Regional::where('location_id',$this->locID)->orderBy('name')->paginate(6);

        return view('livewire.show-locals', [
            'regionals' => $this->regionals,
        ]);
    }
}
