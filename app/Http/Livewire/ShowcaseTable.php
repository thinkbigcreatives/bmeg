<?php

namespace App\Http\Livewire;

use App\Models\Showcase;
use Livewire\Component;
use Livewire\WithPagination;

class ShowcaseTable extends Component
{
    use WithPagination;

    protected $showcases;
    public $search = null;

    public function updating()
    {
        $this->resetPage();
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function render()
    {
        // $this->showcases = !is_null($this->search) ? Showcase::search($this->search)->paginate(5) : Showcase::orderBy('created_at', 'desc')->paginate(5);
        $this->showcases = !is_null($this->search) ? Showcase::search($this->search)->get() : Showcase::orderBy('created_at', 'desc')->get();

        return view('livewire.showcase-table', [
            'showcases' => $this->showcases,
        ]);
    }
}
