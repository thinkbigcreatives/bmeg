<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DirectoryLocation extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'directory_locations';

    public function directories()
    {
        return $this->hasMany(Directory::class);
    }
}
