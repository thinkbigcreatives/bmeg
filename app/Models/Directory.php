<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
    use HasFactory;
    
    protected $guarded = [];
    protected $table = 'directories';

    public function location()
    {
        return $this->belongsTo(DirectoryLocation::class);
    }
}
