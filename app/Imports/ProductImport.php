<?php

namespace App\Imports;

use App\Models\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;

class ProductImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            Product::create([
                'name' => strtoupper($row[0]),
                'description' => $row[1],
                'usage' => isset($row[2]) ? (is_null($row[2]) ? null : $row[2]) : null,
                'category' => $row[3],
                'lazada' => isset($row[4]) ? (is_null($row[4]) ? null : $row[4]) : null,
                'shopee' => isset($row[5]) ? (is_null($row[5]) ? null : $row[5]) : null,
                'website' => isset($row[6]) ? (is_null($row[6]) ? null : $row[6]) : null,
                'image' => isset($row[7]) ? (is_null($row[7]) ? null : $row[7]) : null,
            ]);
        }
    }
}
