<?php

namespace App\Imports;

use App\Models\Directory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;

class DirectoryImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            Directory::create([
                'loc_id' => $row[0],
                'manager' => strtoupper($row[1]),
                'area' => is_null($row[2]) ? null : $row[2],
                'contact' => is_null($row[3]) ? null : $row[3],
                'email' => is_null($row[4]) ? null : $row[4],
            ]);
        }
    }
}
