<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\DirectoryImport;
use Maatwebsite\Excel\Facades\Excel;

class DirectorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new DirectoryImport, 'public/excel/directories.xlsx');
    }
}
