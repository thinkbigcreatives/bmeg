<?php

namespace Database\Seeders;

use App\Models\DirectoryLocation;
use Illuminate\Database\Seeder;

class DirectoryLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = ['North West Luzon','North East Luzon','Central Luzon','South West Luzon','South East Luzon','Visayas','North Mindanao','South Mindanao'];
        
        foreach ($locations as $location) {
            DirectoryLocation::create([
                'name' => $location
            ]);
        }
    }
}
