<?php

namespace Database\Seeders;

use App\Models\Showcase;
use Illuminate\Database\Seeder;

class ShowcaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = ['Test1','Test2','Test3','Test4','Test5','Test6','Test7','Test8',];
        
        foreach ($titles as $key => $title) {
            $temp = $key +1;
            Showcase::create([
                'title' => $title,
                'image' => '0/'.$temp.'.png',
            ]);
        }
    }
}
