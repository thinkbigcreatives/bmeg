<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * 1 - Feeds, 2 - Pointing Supplements, 3 - Supplements, 4 - Shampoo, 5 - Disinfectants, 6 - Antibacterials, 7 - Bundles
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            // $table->double('price')->nullable();
            $table->text('description');
            $table->text('usage')->nullable();
            $table->integer('category')->default(1);
            $table->text('image')->nullable();
            $table->string('shopee')->nullable();
            $table->string('lazada')->nullable();
            $table->string('website')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
